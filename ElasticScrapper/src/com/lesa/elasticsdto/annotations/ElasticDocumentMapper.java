package com.lesa.elasticsdto.annotations;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import com.lesa.simplemappingframework.AnnotationMapper;

public class ElasticDocumentMapper extends AnnotationMapper{
	
	public ElasticDocumentMapper() {
		super(ElasticDocument.class, ElasticField.class);
	}

	public Map<String, String> proccess(Object instance){
		return this.generateMapping(instance.getClass());
	}

	@Override
	protected String proccessField(Field field, Annotation annotation) {
		ElasticField elasticField = (ElasticField) annotation;
		String mapping = elasticField.mapping();
		if (mapping.equals(ElasticField.NULL)) {
			return field.getName();
		}
		return mapping;
	}

	@Override
	protected Map<String, String> proccessClassAnnotation(Annotation annotation, Class<?> klass) {
		ElasticDocument elasticDocument = (ElasticDocument) annotation;
		
		Map<String, String> classMapping = new HashMap<String, String>();
		String type = elasticDocument.type();
		classMapping.put("ElasticType", type);
		if (type.equals(ElasticDocument.NULL)) {
			classMapping.put("ElasticType", klass.getName());
		}
		
		classMapping.put("ElasticIndex", elasticDocument.index());
		
		return classMapping;
		
	}
	
}
