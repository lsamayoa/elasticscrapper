package com.lesa.simplemappingframework;

import java.util.Map;

public interface MapperInterface {
	public Map<String, String> generateMapping(Class<?> klass);
}
