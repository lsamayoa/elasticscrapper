package com.lesa.simplemappingframework;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Map;

public abstract class AnnotationMapper implements MapperInterface{

	private Class<? extends Annotation> classAnnotation;
	private Class<? extends Annotation> fieldAnnotation;

	public AnnotationMapper(Class<? extends Annotation> classAnnotation, Class<? extends Annotation> fieldAnnotation) {
		this.classAnnotation = classAnnotation;
		this.fieldAnnotation = fieldAnnotation;
	}
	
	@Override
	public Map<String, String> generateMapping(Class<?> klass) {
		Annotation annotation = klass.getAnnotation(getClassAnnotation());
		if(annotation == null){
			throw new IllegalArgumentException("Class " + klass + " doesn't have "+ getClassAnnotation() +" Annotation.");
		}
		
		Map<String, String> mappings = proccessClassAnnotation(annotation, klass);
		
		for (Field field: klass.getDeclaredFields()) {
			Annotation annotatedField = field.getAnnotation(getFieldAnnotation());
			if (annotatedField == null) {
				continue;
			}
			String mapping = proccessField(field, annotatedField);
			
			mappings.put(field.getName(), mapping);
		}
		return mappings;
	}
	
	protected abstract Map<String, String> proccessClassAnnotation(Annotation annotation, Class<?> klass);
	
	protected abstract String proccessField(Field field, Annotation annotation);

	public Class<? extends Annotation> getClassAnnotation() {
		return classAnnotation;
	}

	public void setClassAnnotation(Class<? extends Annotation> classAnnotation) {
		this.classAnnotation = classAnnotation;
	}

	public Class<? extends Annotation> getFieldAnnotation() {
		return fieldAnnotation;
	}

	public void setFieldAnnotation(Class<? extends Annotation> fieldAnnotation) {
		this.fieldAnnotation = fieldAnnotation;
	}

}
