package com.lesa.simplemappingframework;

import java.util.HashMap;
import java.util.Map;

public class MappingCache {

	
	private Map<String, Map<String, String>> mappings;
	
	public MappingCache() {
		mappings = new HashMap<String, Map<String, String>>();
	}

	public void save(String className, Map<String, String> mapping) {
		getMappings().put(className, mapping);
	}
	
	public Map<String, String> get(String className){
		return getMappings().get(className);
	}

	public Map<String, Map<String, String>> getMappings() {
		return mappings;
	}

	public void setMappings(Map<String, Map<String, String>> mappings) {
		this.mappings = mappings;
	}

}
