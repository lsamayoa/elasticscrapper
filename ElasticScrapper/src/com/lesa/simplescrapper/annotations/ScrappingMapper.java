package com.lesa.simplescrapper.annotations;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import com.lesa.simplemappingframework.AnnotationMapper;

public class ScrappingMapper extends AnnotationMapper {

	public ScrappingMapper() {
		super(Scrappable.class, ScrappableField.class);
	}

	public Map<String, String> proccess(Object instance) {
		return this.generateMapping(instance.getClass());
	}

	@Override
	protected String proccessField(Field field, Annotation annotation) {
		ScrappableField elasticField = (ScrappableField) annotation;
		String mapping = elasticField.mapping();
		if (mapping.equals(ScrappableField.NULL)) {
			return field.getName();
		}
		return mapping;
	}
	
	@Scrappable
	static class Test{
		@ScrappableField
		String queBuenaShit;
	}
	
	public static void main(String[] args) {
		System.out.println(new ScrappingMapper().proccess(new Test()));
	}

	@Override
	protected Map<String, String> proccessClassAnnotation(Annotation annotation, Class<?> klass) {
		return new HashMap<String, String>();
	}
}
