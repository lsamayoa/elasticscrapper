package com.lesa.elastisearch;

import java.util.HashMap;
import java.util.Map;

import org.elasticsearch.client.Client;

import com.google.gson.Gson;
import com.lesa.simplemappingframework.MapperInterface;

public class GsonEntityManager implements EntityManager{

	private MapperInterface mapper;
	private Client client;
	private Gson gson;
	
	private Map<String, Map<String, String>> cache;
	
	public GsonEntityManager(MapperInterface mapper, Client client, Gson gson){
		this.mapper = mapper;
		this.client = client;
		this.gson = gson;
		this.cache = new HashMap<>();
	}
	
	protected Map<String, String> getMapping(Object obj){
		
		Class<?> klass = obj.getClass();
		String className = klass.getName();
		Map<String, String> mapping;
		if(cache.containsKey(className)){
			mapping = cache.get(klass.getName());
			if (mapping != null) {
				return mapping;
			}
		}
		
		mapping = mapper.generateMapping(klass);
		cache.put(klass.getName(), mapping);
		
		return mapping;
	}
	
	@Override
	public void save(Object obj) {
		Map<String, String> mapping = getMapping(obj);
		client.prepareIndex(mapping.get("ElasticIndex"), mapping.get("ElasticType")).setSource(gson.toJson(obj)).execute();
	}
	
	protected String createDocument(Object obj){
		return gson.toJson(obj);
	}

	public MapperInterface getMapper() {
		return mapper;
	}

	public void setMapper(MapperInterface mapper) {
		this.mapper = mapper;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

}
